#!/usr/bin/python3

import requests
import datetime

ISSURL = "http://api.open-notify.org/iss-now.json"

def main():
    iss = requests.get(ISSURL)
    iss = iss.json()
    #epoch = (iss['iss_position']['timestamp'])
    #date_time = datetime.datetime.fromtimestamp(epoch)

    print('CURRENT LOCATION OF THE ISS:')
    print(f"Time-stamp: {datetime.datetime.fromtimestamp(iss['timestamp'])}")
    print(f"Longitude: {iss['iss_position']['longitude']}")
    print(f"Latitude:  {iss['iss_position']['latitude']}")

if __name__ == "__main__":
    main()
